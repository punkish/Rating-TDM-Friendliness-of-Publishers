On the ContentMine Slack channel, Chris Hartgerink made an off-hand comment wishing for a "ContentMine award as badge of honor" for TDM-friendly publishers. I don't know how serious Chris was, but I think that is a great idea. I offer a 5-star rubric to assess how TDM-friendly a publisher is  across legal, semantic and technical factors:

★ Offers content under an open license  
★ There are no restrictions on the use of the output  
★ A RESTful API is provided  
★ The API is fully-documented (Swagger, I/O, homegrown, doesn't matter) and is browsable (HATEOAS)  
★ There are no throttling limits

<img class="bar" src="badge.png" width="300">

The stars are mutually exclusive, so it is possible to have 0 to 5 stars. Fewer than 3 stars make the publisher unfriendly, and three or more stars make the publisher TDM-friendly.

This is just an initial suggestion. Have at it, make it even better, then create an attractive badge (SVG, PNG, etc.) that can incrementally depict one to five stars, add explanatory text to it, and hang it on the CM wall. Then let's start using it. The world will catch up with us.
